const fs = require("fs-extra");
const workboxBuild = require('workbox-build');

const importHTML = path => fs.readFileSync(path, "utf8");

const importHTMLs = dir =>
  fs.readdirSync(dir)
    .reduce((acc, v) => [ ...acc, importHTML(`${dir}/${v}`) ], []);

const [ before, after ] = importHTMLs("./shell");
const shellWrap = x => before + x + after;

fs.ensureDirSync("./public");

fs.readdirSync("./pages")
  .reduce((acc, v) => {
    return [
      ...acc,
      {
        name: v,
        data: importHTML(`./pages/${v}`)
      }
    ]
  }, [])
  .forEach(({ name, data }) => {
    fs.writeFileSync(`./public/${name}`, shellWrap(data));
  });

fs.copySync("./assets/", "./public/assets/")
fs.copySync("./styles/", "./public/styles/")
fs.copySync("./scripts/", "./public/scripts/")

workboxBuild.injectManifest({
  swSrc: "./scripts/sw.js",
  swDest: "./public/sw.js",
  globDirectory: "./public/",
  globPatterns: [
    "**/*.{html,css,js,png,jpg,svg}",
  ]
}).then(({count, size, warnings}) => {
  warnings.forEach(console.warn);
  size = (size / 1000 / 1000).toFixed(2)
  const msg = `${count} files will be precached, totaling ${size} MB.`;
  console.log(msg);
})
